from conexion import Conexion
from cursor_del_pool import CursorDelPool
from persona import Persona
from logger_base import  log

class PersonaDao:
    '''
    DAO DATA ACCESS OBJECT
    CRUD CREATE, READ, UpDATE AND DELETE
    '''
    _SELECCIONAR = 'SELECT * FROM persona ORDER BY id_persona'
    _INSERTAR = 'INSERT INTO persona(nombre,apellido,email) VALUES(%s,%s,%s)'
    _ACTUALIZAR = 'UPDATE persona SET nombre=%s, apellido=%s, email=%s WHERE id_persona=%s'
    _ELIMINAR = 'DELETE FROM persona WHERE id_persona=%s'

    @classmethod
    def seleccionar(cls):
        with CursorDelPool() as cursor:
            cursor.execute(cls._SELECCIONAR)
            #Para recuperar todos los registros con fetchall
            registros = cursor.fetchall()
            #Para recorrer los registros la cual es una lista
            personas = []
            for registro in registros:
                persona = Persona(registro[0],registro[1],registro[2],registro[3])
                personas.append(persona) #append para agregar el objeto tipo persona
            return personas

    @classmethod
    # recibiremos un objeto de tipo persona para insertarlo a la db
    def insertar(cls, persona):
        with CursorDelPool() as cursor:
            valores = (persona.nombre,persona.apellido,persona.email)
            cursor.execute(cls._INSERTAR,valores)
            log.debug(f'Persona insertada: {persona}')
            return cursor.rowcount

    @classmethod
    def actualizar(cls,persona):
        with CursorDelPool() as cursor:
            valores = (persona.nombre, persona.apellido, persona.email, persona.id_persona)
            cursor.execute(cls._ACTUALIZAR,valores)
            log.debug(f'Persona actualizada: {persona}')
            return cursor.rowcount

    @classmethod
    def eliminar(cls,persona):
        with CursorDelPool() as cursor:
            #se agrega una como al final ya que es una tupla
            valores =(persona.id_persona,)
            cursor.execute(cls._ELIMINAR,valores)
            log.debug(f'Elementos eliminados: {persona}')
            return cursor.rowcount

if __name__ == '__main__':
    #insertar un registro;
    persona1 = Persona(id_persona='',nombre='Alejandra',apellido='Tellez',email='atellez@outlook.com')
    personas_insertadas = PersonaDao.insertar(persona1)
    log.debug(f'Personas insertadas: {personas_insertadas}')

    #ACtualizar un registro:
    persona1 = Persona(1,'Alvaro','Obregon','aobregon@outlook.com')
    personas_actualizadas = PersonaDao.actualizar(persona1)
    log.debug(f'Personas actualizadas: {personas_actualizadas}')

    #Eliminar un registro:
    persona1 = Persona(id_persona=13)
    personas_eliminadas = PersonaDao.eliminar(persona1)
    log.debug(f'Personas eliminadas: {personas_eliminadas}')

    #seleccionar objeto
    personas = PersonaDao.seleccionar()
    for personas in personas:
        log.debug(personas)


